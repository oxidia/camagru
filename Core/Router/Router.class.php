<?php
namespace Core\Router;

class Router
{
	private $url;
	private $routes;

	public function __construct($url)
	{
		$this->url = $url;
		$this->routes = [];
	}

	public function get($path, $callable, $action = null)
	{
		return ($this->add($path, $callable, $action, 'GET'));
	}

	public function post($path, $callable, $action = null)
	{
		return ($this->add($path, $callable, $action, 'POST'));
	}

	public function redirect($route)
	{
		header("location: $route");
	}

	public function start()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if (isset($this->routes[$method]) === false)
			throw new RouterException("Unsuported REQUEST_METHOD `$method`");
		$routes = $this->routes[$method];
		foreach ($routes as $route)
		{
			if ($route->match($this->url))
				return ($route->call());
		}
		return (false);
	}

	/*
	** private methodes
	*/

	/*
	** add a route to the routes list
	*/
	private function add($path, $callable, $action, $method)
	{
		$route = new Route($path, $callable, $action);
		$this->routes[$method][] = $route;
		return ($route);
	}
}
