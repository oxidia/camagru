<?php
namespace Core;

class Model
{
	protected   $_database;

	public function __construct()
	{
		$this->_database = Database::init(DB_DSN, DB_USER, DB_PASSWORD);
	}

	final public function getDatabaseObject()
	{
		return ($this->_database);
	}
}
