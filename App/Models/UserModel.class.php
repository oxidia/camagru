<?php
namespace App\Models;

class UserModel extends \Core\Model
{
	public function getImages($owner_id = null)
	{
		if ($owner_id !== null)
		{
			return ($this->getDatabaseObject()->selectQuery('SELECT * FROM `images` WHERE `owner_id` = ? ORDER BY `creation_date` DESC', [
				$owner_id
			]));
		}
		return ($this->getDatabaseObject()->selectQuery('SELECT * FROM `images`'));
	}

	public function getImagesByPage($offset, $max)
	{
		return ($this->getDatabaseObject()->selectQuery('SELECT * FROM `images` ORDER BY `creation_date` DESC LIMIT ?, ?', [
			$offset, $max
		]));
	}

	public function getImageById($id)
	{
		$rows = $this->getDatabaseObject()->selectQuery('SELECT * FROM `images` WHERE `id` = ?', [
			$id
		]);
		if ($rows === false || count($rows) === 0)
			return (null);
		return ($rows[0]);
	}

	public function getCommentById($id)
	{
		$rows = $this->getDatabaseObject()->selectQuery('SELECT * FROM `comments` WHERE `id` = ?', [
			$id
		]);
		if ($rows === false || count($rows) === 0)
			return (null);
		return ($rows[0]);
	}

	public function deleteComment($id)
	{
		$res = $this->getDatabaseObject()->nonQuery('DELETE FROM `comments` WHERE `id` = ?', [
			$id
		]);
		return ($res);
	}
	
	public function deleteImage($id)
	{
		$db = $this->getDatabaseObject();
		$res = $db->nonQuery('DELETE FROM `comments` WHERE `image_id` = ?', [
			$id
		]);
		if ($res === false)
			return (false);
		$res = $db->nonQuery('DELETE FROM `user_reactions` WHERE `image_id` = ?', [
			$id
		]);
		if ($res === false)
			return (false);
		$res = $db->nonQuery('DELETE FROM `images` WHERE `id` = ?', [
			$id
		]);
		return ($res);
	}

	public function getComments($image_id)
	{
		return ($this->getDatabaseObject()->selectQuery('SELECT `comments`.`id` AS `comment_id`, `users`.`id` AS `owner_id`, `users`.`username`, `comments`.`text`, DATE_FORMAT(`comments`.`creation_date`, "%Y %M %d %H:%i") AS `date` FROM `comments` INNER JOIN `users` ON `comments`.`owner_id` = `users`.`id` WHERE `image_id` = ? ORDER BY `comments`.`creation_date` DESC', [
			$image_id
		]));
	}

	public function getFilters()
	{
		return ($this->getDatabaseObject()->selectQuery('SELECT * FROM `filters`'));
	}

	function getUsers($field, $value)
	{
		return ($this->getDatabaseObject()->selectQuery("SELECT * FROM `users` WHERE `$field` = ?", [
			$value
		]));
	}

	public function insertUserImage($path)
	{
		$user = json_decode($_SESSION['user_data']);
		$res = $this->getDatabaseObject()->nonQuery("INSERT INTO `images`(`owner_id`, `creation_date`, `filename`, `text`) VALUES (?, NOW(), ?, ?)", [
			$user->id, $path, ''
		]);
		return ($res);
	}

	public function getFilterById($id)
	{
		$res = $this->getDatabaseObject()->selectQuery('SELECT * FROM `filters` WHERE id = ?', [
			$id
		]);
		if (count($res) === 0)
			return (false);
		return ($res[0]);
	}

	public function getReactions()
	{
		$rows = $this->getDatabaseObject()->selectQuery('SELECT * FROM `reactions`');
		$res = [];
		foreach ($rows as $row)
			$res[$row['label']] = $row['id'];
		return ($res);
	}

	public function getImageReactionsDetails($id_image, $id_current_user)
	{
		$result = [];
		$id_current_user = intval($id_current_user);
		$rows = $this->getDatabaseObject()->selectQuery('SELECT `owner_id`, `reaction_id`, `label` FROM `user_reactions` INNER JOIN `reactions` ON `reactions`.`id` = `user_reactions`.`reaction_id` WHERE `image_id` = ?', [
			$id_image
		]);
		foreach ($rows as $row)
		{
			if (!array_key_exists($row['label'], $result))
			{
				$result[$row['label']] = [
					'count' => 1,
					'reaction_id' => $row['reaction_id'],
					'has_current_user' => $id_current_user === intval($row['owner_id'])
				];
			}
			else
			{
				$result[$row['label']]['count']++;
				if ($result[$row['label']]['has_current_user'] === false)
					$result[$row['label']]['has_current_user'] = $id_current_user === intval($row['owner_id']);
			}
		}
		return ($result);
	}

	public function getUserByEmail($email)
	{
		$res = $this->getDatabaseObject()->selectQuery('SELECT * FROM `users` WHERE `email` LIKE ?', [
			$email
		]);
		if ($res === false || count($res) === 0)
			return (false);
		return ($res[0]);
	}

	public function sendResetRequest($email)
	{
		$token = generate_token([ $email ]);
		$res = $this->getDatabaseObject()->nonQuery('UPDATE `users` SET `token` = ? WHERE `email` LIKE ?', [
			$token, $email
		]);
		if ($res === true)
		{
			$body = "<a href=http://" . SERVER_DOMAIN . "/reset/$email/$token>Reset</a>";
			return (mail($email, 'Forgot password', $body, [
				'From' => '    ybahlaou@camagru.info    '
			]));
		}
		return ($res);
	}

	public function reset($email, $token, $pass)
	{
		$user = $this->getUserByEmail($email);
		if ($user === false)
			return (false);
		if ($user['token'] !== $token)
			return (false);
		$res = $this->getDatabaseObject()->nonQuery('UPDATE `users` SET `token` = ?, `password` = ?', [
			'', hash_password($pass)
		]);
		return ($res);
	}

	public function register($email, $username, $password)
	{
		// https://www.digitalocean.com/community/tutorials/how-to-install-and-configure-postfix-as-a-send-only-smtp-server-on-debian-9
		$activation_token = generate_token([ $email, $username ]);
		$res = $this->getDatabaseObject()->nonQuery('INSERT INTO `users`(`email`, `username`, `password`, `token`) VALUES (?, ?, ?, ?)', [
			$email, $username, hash_password($password), $activation_token
		]);
		if ($res !== false)
		{
			$body = "<a href=http://" . SERVER_DOMAIN . "/activate/$activation_token>Activate</a>";
			return (mail($email, 'Account activation', $body, [
				'From' => '    ybahlaou@camagru.info    '
			]));
		}
		return ($res);
	}

	public function activateAccount($email, $token)
	{
		$rows = $this->getDatabaseObject()->nonQuery('SELECT * FROM `users` WHERE `email` LIKE ?', [
			$email
		]);
		if ($rows === false || count($rows) === 0)
			return (false);
		$rows = $rows[0];
		if ($row['token'] === $token)
		{
			$rows = $this->getDatabaseObject()->nonQuery('UPDATE `users` SET `is_active` = 0, `token` = NULL WHERE `email` LIKE ?', [
				$email
			]);
			return ($rows);
		}
		return (false);
	}

	public function react($id_image, $id_reaction, $id_owner, &$is_set)
	{
		$rows = $this->getDatabaseObject()->selectQuery('SELECT * FROM `reactions` WHERE `id` = ?', [
			$id_reaction
		]);
		if (count($rows) === 0)
			return (false);
		$rows = $this->getDatabaseObject()->selectQuery('SELECT * FROM `images` WHERE `id` = ?', [
			$id_image
		]);
		if (count($rows) === 0)
			return (false);
		$rows = $this->getDatabaseObject()->selectQuery('SELECT * FROM `user_reactions` WHERE `owner_id` = ? AND `image_id` = ?  AND `reaction_id` = ?', [
			$id_owner, $id_image, $id_reaction
		]);
		if (count($rows) === 0)
		{
			$res = $this->getDatabaseObject()->nonQuery("INSERT INTO `user_reactions`(`owner_id`, `image_id`, `reaction_id`) VALUES (?, ?, ?)", [
				$id_owner, $id_image, $id_reaction
			]);
			$is_set = true;
		}
		else
		{
			$res = $this->getDatabaseObject()->nonQuery('DELETE FROM `user_reactions` WHERE `owner_id` = ? AND `image_id` = ?  AND `reaction_id` = ?', [
				$id_owner, $id_image, $id_reaction
			]);
			$is_set = false;
		}
		return ($res);
	}

	public function comment($id_image, $text, $user)
	{
		$image = $this->getDatabaseObject()->selectQuery('SELECT * FROM `images` WHERE `id` = ?', [
			$id_image
		]);
		if (count($image) === 0)
			return (false);
		$image = $image[0];
		$res = $this->getDatabaseObject()->nonQuery('INSERT INTO `comments`(`owner_id`, `image_id`, `text`, `creation_date`) VALUES (?, ?, ?, NOW())', [			
			$user->id, $id_image, $text
		]);
		if ($res !== false && $user->id != $image['owner_id'])
		{
			$owner = $this->getDatabaseObject()->selectQuery('SELECT * FROM `users` WHERE `id` = ?', [
				$image['owner_id']
			]);
			$owner = $owner[0];
			if ($owner['notification'] === 1)
				$res = mail($owner['email'], 'Image comment', 'One of your images commented by `' . $user->username . '`', [
					'From' => '    ybahlaou@camagru.info    '
				]);
		}
		return ($res);
	}

	public function getImagesCount()
	{
		$rows = $this->getDatabaseObject()->selectQuery('SELECT COUNT(*) AS `count` FROM `images`');
		if ($rows !== false)
			return ($rows[0]['count']);
		return (false);
	}

	public function update($email, $uname, $notif, $pass, $id)
	{
		$pass = hash_password($pass);
		return ($this->getDatabaseObject()->nonQuery('UPDATE `users` SET `email` = ?, `username` = ?, `password` = ?, `notification` = ? WHERE `id` = ?', [
			$email, $uname, $pass, $notif, $id
		]));
	}

	/*
	** $username string
	** $password string
	** @return false | array
	** @return false if $username and $password does not match in database, otherwise array of user informations
	*/
	public function auth($username, $password)
	{
		$rows = $this->getDatabaseObject()->selectQuery('SELECT * FROM `users` WHERE `username` LIKE ? AND `password` LIKE ?', [
			$username, hash_password($password)
		]);
		if ($rows == false || count($rows) === 0)
			return (false);
		return ($rows[0]);
	}
}
