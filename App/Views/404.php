<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">

	<title>404 Not Found</title>

	<link rel="stylesheet" type="text/css" href="<?= C_CSS ?>/ini.css" />
	<link rel="stylesheet" type="text/css" href="<?= C_CSS ?>/404.css" />
</head>
<body>
	<h1><label>Error 404</label></h1>
	<h3>Oops you are lost, go <a href="/">Home</a></h3>
</body>
</html>