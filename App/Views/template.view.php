<?php
if (!isset($css_links))
	$css_links = [];
?>
<!DOCTYPE html>
<html lang="en" direction="ltr">
<head>
	<meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?= $pageTitle ?></title>

	<link rel="stylesheet" type="text/css" href="<?= C_CSS ?>/ini.css" />
	<link rel="stylesheet" type="text/css" href="<?= C_CSS ?>/lib.css" />
	<link rel="stylesheet" type="text/css" href="<?= C_CSS ?>/grid.css" />
	<link rel="stylesheet" type="text/css" href="<?= C_CSS ?>/controls.css" />
	<link rel="stylesheet" type="text/css" href="<?= C_CSS ?>/nav.css" />
<?php foreach ($css_links as $value): ?>
	<link rel="stylesheet" type="text/css" href="<?= C_CSS . '/' . $value ?>" />
<?php endforeach; ?>
	<script src="<?= C_JS ?>/lib.js"></script>
	<script src="<?= C_JS ?>/dom.js"></script>
	<script src="<?= C_JS ?>/ajax.js"></script>
</head>
<body>
	<?= $navbar ?>
	<?= $content ?>
	<?= $footer ?>
</body>
</html>
