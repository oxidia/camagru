<section class="container form">
	<div id="error">
	</div>
	<section class="body">
		<div id="message" class="message">
			<label class="text"></label>
		</div>
		<div class="control">
			<label class="label">E-mail</label>
			<input class="textbox" id="input_email" type="text" />
		</div>
		<div class="control">
			<button class="button" id="btn_reset">Reset</button>
		</div>
	</section>
</section>
<script src="<?= C_JS ?>/sendreset.js"></script>