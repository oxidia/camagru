<?php
$css_links = [
    'activate.css'
];
$user_data = json_decode($_SESSION['user_data']);
?>
<h1>Hey <?= $user_data->username ?></h1>
<p>Go to your email `<?= $user_data->email ?>` and click the activate link to activate your account.</p>
