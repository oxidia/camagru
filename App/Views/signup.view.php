<section class="container form">
	<div id="error">
	</div>
	<section class="body">
		<div id="message" class="message">
			<label class="text"></label>
		</div>
		<div class="control">
			<label class="label">E-mail</label>
			<input class="textbox" id="input_email" type="text" />
		</div>
		<div class="control">
			<label class="label">Username</label>
			<input class="textbox" id="input_uname" type="text" />
		</div>
		<div class="control">
			<label class="label">Password</label>
			<input class="textbox" id="input_pass" type="password" />
		</div>
		<div class="control">
			<button class="button" id="btn_signup">Signup</button>
		</div>
	</section>
</section>
<script src="<?= C_JS ?>/signup.js"></script>