<?php
$css_links = [
	'create.css'
];
?>
<div>
	<section class="container form">
		<div id="message" class="message">
			<label class="text"></label>
		</div>
		<section id="capture_section">
			<main>
				<div>
					<div id="video_container" class="fill">
						<video class="active" id="video"></video>
						<canvas class="hidden" id="image_to_upload"></canvas>
					</div>
				</div>
				<div class="imgs">
				<?php foreach ($filters as $filter): ?>
					<div class="img filter" data-selected="true" data-id="<?= $filter['id'] ?>">
						<div class="img-inner" style="background-image: url(http://localhost/filter/<?= basename($filter['filename'], '.png') ?>)" ></div>
					</div>
				<?php endforeach; ?>
				</div>
				<div class="controls">
					<button id="btn_capture" disabled class="button">Capture</button>
					<label class="button">
						Choose file
						<input type="file" id="input_file" class="hidden" />
					</label>
					<button id="btn_save" disabled class="button">Save</button>
					<button id="btn_cancel" class="button">Cancel</button>
					<button id="btn_show" class="button">Show</button>
				</div>
			</main>
		</section>
	</section>
	<div id="side" class="side">
		<div id="btn_close" class="btn-close">&times;</div>
		<main id="list_images">
		<?php
		foreach ($images as $image):
		?>
			<div class="image">
				<img src="<?= C_USERS_IMGS . '/' . $image['filename'] ?>" alt="image" />
				<div class="control footer">
					<button class="button" data-role="delete" data-id="<?= $image['id'] ?>">Delete</button>
				</div>
			</div>
		<?php
		endforeach;
		?>
		</main>
	</div>
</div>
<script src="<?= C_JS ?>/create.js"></script>
