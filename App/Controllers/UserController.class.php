<?php
namespace App\Controllers;

class UserController extends \Core\Controller
{
	public function signup()
	{
		if (isset($_POST['uname']) && isset($_POST['email']) && isset($_POST['pass']))
		{
			$message = '';
			$email = validate_email($_POST['email']);
			$username = validate_username($_POST['uname']);
			$password = validate_password($_POST['pass']);
			if ($email === false)
				$message = 'Invalid email.';
			if ($username === false)
				$message .= 'Invalid username supported charset (alpha-numeric, - and _).';
			if ($password === false)
				$message .= 'Invalid passsword (length: between 6 and 20, contain at least one special char).';
			if ($email !== false && $username !== false && $password !== false)
			{
				$model = $this->loadModel('User');
				$rows = $model->getDatabaseObject()->selectQuery("SELECT * FROM `users` WHERE `username` LIKE ? OR `email` LIKE ?", [
					$username, $email
				]);
				if ($rows !== false && count($rows) === 0)
				{
					if ($this->_data['json_data'] = $model->register($email, $username, $password) === false)
						$json_data = json_data(1, 'Something went wrong.', '');
					else
						$json_data = json_data(0, 'User signed up.', '');
				}
				else
				{
					$message = $email === $rows[0]['email'] ? "Email `$email` already exists." : "Username `$username` already exists.";
					$json_data = json_data(2, $message, '');
				}
			}
			else
				$json_data = json_data(1, $message, '');
			$this->_data['json_data'] = $json_data;
			$this->renderApi('signup');
		}
	}

	public function signin()
	{
		if (isset($_POST['uname']) && isset($_POST['pass']))
		{
			$model = $this->loadModel('User');
			$username = $_POST['uname'];
			$password = $_POST['pass'];
			if (($row = $model->auth($username, $password)) === false)
				$json_data = json_data(2, 'Username and/or password are wrong.', '');
			else
			{
				$json_data = json_data(0, 'User signed in.', $row);
				$_SESSION['user_data'] = json_encode($row);
			}
			$this->_data['json_data'] = $json_data;
			$this->renderApi('signin');
		}
	}

	public function activateAccount($token)
	{
		$model = $this->loadModel('User');
		$rows = $model->getDatabaseObject()->selectQuery("SELECT * FROM `users` WHERE `token` LIKE ?", [
			$token
		]);
		if ($rows === false || count($rows) === 0)
			return (false);
		if ($rows[0]['token'] !== $token)
			return (false);
		$result = $model->getDatabaseObject()->nonQuery("UPDATE `users` SET `is_active` = TRUE, `token` = '' WHERE `token` LIKE ?", [
			$token
		]);
		return ($result);
	}

	public function react()
	{
		$error = !is_activated();
		if ($error === false)
			$error = !isset($_POST['image_id']) || !isset($_POST['reaction_id']);
		if ($error === false)
		{
			$id_image = intval($_POST['image_id']);
			$id_reaction = intval($_POST['reaction_id']);
			$user_data = json_decode($_SESSION['user_data']);
			$model = $this->loadModel('User');
			$error = !$model->react($id_image, $id_reaction, $user_data->id, $is_set);
		}
		if ($error === true)
			$json_data = json_data(1, 'something went wrong', '');
		else
			$json_data = json_data(0, 'success', [
				'is_set' => $is_set
			]);
		print($json_data);
	}

	public function comment()
	{
		$error = !is_activated();
		$message = 'success';
		if ($error === false)
			$error = !isset($_POST['image_id']) || !isset($_POST['text']);
		else
			$message = 'error';
		if ($error === false)
		{
			$id_image = intval($_POST['image_id']);
			$text = trim($_POST['text']);
			$error = strlen($text) === 0;
			if ($error === false)
			{
				$user_data = json_decode($_SESSION['user_data']);
				$model = $this->loadModel('User');
				$error = !$model->comment($id_image, $text, $user_data);
				if ($error === true)
					$message = 'something went wrong';
			}
			else
				$message = 'Empty comment.';
		}
		else
			$message = 'something went wrong';
		if ($error === true)
			$json_data = json_data(1, $message, '');
		else
		{
			$model = $this->loadModel('User');
			$comment = $model->getComments($id_image)[0];
			$json_data = json_data(0, $message, [ 'comment' => $comment]);
		}
		print($json_data);
	}

	public function deleteComment()
	{
		$model = $this->loadModel('User');
		$error = !is_activated();
		if ($error === false)
			$error = !isset($_POST['comment_id']);
		if ($error === false)
		{
			$user_data = json_decode($_SESSION['user_data']);
			$comment_id = intval($_POST['comment_id']);
			$comment = $model->getCommentById($comment_id);
			$error = $comment === null || $comment['owner_id'] !== $user_data->id;
			if ($error === false)
				$model->deleteComment($comment_id);
		}
		if ($error === false)
			print(json_data(0, 'success', ''));
		else
			print(json_data(1, 'something went wrong', ''));
	}

	public function deleteImage()
	{
		$model = $this->loadModel('User');
		$error = !is_activated();
		if ($error === false)
			$error = !isset($_POST['id']);
		if ($error === false)
		{
			$user_data = json_decode($_SESSION['user_data']);
			$image_id = intval($_POST['id']);
			$error = ($image = $model->getImageById($image_id)) === null;
			if ($error === false)
				$error = $image['owner_id'] != $user_data->id;
			if ($error === false)
				$error = !$model->deleteImage($image_id);
		}
		if ($error === false)
			print(json_data(0, 'success', [ 'image' => $image ]));
		else
			print(json_data(1, 'something went wrong', ''));
	}

	public function sendResetRequest()
	{
		$error = !isset($_POST['email']);
		$message = 'Go to your email to change your password.';
		if ($error === false)
		{
			$model = $this->loadModel('User');
			$email = validate_email($_POST['email']);
			$error = $email === false;
			if ($error === false)
			{
				$error = ($user = $model->getUserByEmail($email)) === false;
				if ($error === false)
					$model->sendResetRequest($email);
				else
					$message = 'E-mail does not exists';
			}
			else
				$message = 'E-mail does not exists';
		}
		else
			$message = 'something went wrong';
		if ($error === false)
			print(json_data(0, $message, ''));
		else
			print(json_data(1, $message, ''));
	}

	public function reset()
	{
		$model = $this->loadModel('User');
		$error = !isset($_POST['email']) || !isset($_POST['token']) || !isset($_POST['pass']);
		$message = 'success';
		if ($error === false)
		{
			$email = validate_email($_POST['email']);
			$pass = validate_password($_POST['pass']);
			$token = $_POST['token'];
			$error = $email === false || $pass === false;
			if ($pass === false)
				$message = 'Invalid passsword (length: between 6 and 20, contain at least one special char).';
			if ($error === false)
			{
				$error = !$model->reset($email, $token, $pass);
				if ($error === true)
					$message = 'something went wrong';
			}
		}
		else
			$message = 'something went wrong';
		if ($error === false)
			print(json_data(0, $message, ''));
		else
			print(json_data(1, $message, ''));
	}

	public function update()
	{
		$error = !is_activated();
		$message = 'Updated';
		if ($error === false)
			$error = !isset($_POST['email']) || !isset($_POST['uname']) || !isset($_POST['notif']) ||
					!isset($_POST['pass']) || !isset($_POST['newpass']);
		if ($error === false)
		{
			$user_data = json_decode($_SESSION['user_data']);
			$email = validate_email($_POST['email']);
			$uname = validate_username($_POST['uname']);
			$pass = $_POST['pass'];
			$newpass = $_POST['newpass'];
			$notif = $_POST['notif'] == 'true' ? 1 : 0;
			$model = $this->loadModel('User');
			if ($newpass === '')
				$newpass = $pass;
			$newpass = validate_password($newpass);
			if ($email === false)
				$message = 'Invalid email.';
			if ($uname === false)
				$message .= 'Invalid username supported charset (alpha-numeric, - and _).';
			if ($newpass === false)
				$message .= 'Invalid passsword (length: between 6 and 20, contain at least one special char).';
			$error = hash_password($pass) !== $user_data->password;
			if ($error === false)
			{
				$error = $email === false || $uname === false || $newpass === false;
				if ($error === false)
				{
					if ($model->update($email, $uname, $notif, $newpass, $user_data->id))
						$_SESSION['user_data'] = json_encode($model->auth($uname, $newpass));
					else
						$message = 'Something went wrong';
				}
			}
			else
				$message = 'Wrong password';
		}
		else
			$message = 'Something went wrong';
		if ($error === true)
			print(json_data(1, $message, []));
		else
			print(json_data(0, $message, []));
	}

	public function signinView()
	{
		$this->renderView('signin');
	}

	public function signupView()
	{
		$this->renderView('signup');
	}

	public function createView()
	{
		if (!is_connected())
			return (false);
		$user_data = json_decode($_SESSION['user_data']);
		$model = $this->loadModel('User');
		$filters = $model->getFilters();
		$images = $model->getImages($user_data->id);
		$data = [];
		if ($filters !== false)
			$data['filters'] = $filters;
		if ($images !== false)
			$data['images'] = $images;
		$this->renderView('create', $data);
	}

	public function profileView()
	{
		if (!is_connected())
			return (false);
		$user_data = json_decode($_SESSION['user_data']);
		$this->renderView('profile', [ 'user_data' => $user_data ]);
	}

	public function galleryView($page)
	{
		if ($page <= 0)
			$page = 1;
		$model = $this->loadModel('User');
		$images = $model->getImagesByPage(($page - 1) * MAX_IMAGES_PAGE, MAX_IMAGES_PAGE);
		$data = [];
		if ($images !== false)
			$data['images'] = $images;
		$this->renderView('gallery', $data);
	}

	public function activateView()
	{
		$this->renderView('activate');
	}

	public function sendResetView()
	{
		$this->renderView('sendreset');
	}

	public function resetView($params)
	{
		$this->renderView('reset', $params);
	}
}
