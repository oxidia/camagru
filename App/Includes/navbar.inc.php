<nav class="container">
	<ul>
		<li><a title="gallery" href="/gallery/1" class="nav-img gallery"></a></li>
	<?php
	if ($_SESSION['user_data'] === null):
	?>
		<li class="right last"><a title="signin" href="/signin" class="nav-img signin"></a></li>
		<li class="right"><a title="signup" href="/signup" class="nav-img signup"></a></li>
	<?php
	else:
	?>
		<li><a title="camera" href="/create" class="nav-img camera"></a></li>
		<li class="right last"><a title="signout" href="/signout" class="nav-img signout" ></a></li>
		<li class="right"><a title="profile" href="/profile" class="nav-img profile"></a></li>
	<?php
	endif;
	?>
	</ul>
</nav>
