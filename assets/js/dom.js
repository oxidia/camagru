'use strict';
(function(owner) {
	function defineDom(selector) {
		var elements = [];

		if (selector instanceof HTMLElement || selector instanceof HTMLDocument || selector instanceof Window)
			elements.push(selector);
		else if (selector instanceof NodeList || selector instanceof HTMLCollection)
			elements = Array.from(selector);
		else
			elements = document.querySelectorAll(selector);

		return {
			'on': function(params) {
				elements.forEach(function(elm) {
					if (isNull(elm))
						return;
					for (var key in params) {
						elm.addEventListener(key, params[key]);
					}
				});
			},
			'load': function(callable) {
				this.on({
					'load': callable
				});
			},
			'click': function(callable) {
				this.on({
					'click': callable
				});
			},
			'play': function() {
				elements.forEach(function(elm) {
					if (isFunction(elm.play))
						elm.play();
				});
			},
			'width': function() {
				if (elements.length !== 0)
					return (elements[0].clientWidth);
			},
			'height': function() {
				if (elements.length !== 0)
					return (elements[0].clientHeight);
			},
			'addClass': function(value) {
				if (!isNull(value)) {
					elements.forEach(function(elm) {
						elm.classList.add(value);
					});
				}
			},
			'removeClass': function(value) {
				elements.forEach(function(elm) {
					if (isNull(value))
						elm.removeAttribute('class');
					else
						elm.classList.remove(value);
				});
			},
			'toggleClass': function(value) {
				if (!isNull(value)) {
					elements.forEach(function(elm) {
						elm.classList.toggle(value);
					});
				}
			},
			'css': function(property, value = null) {
				if (isNull(value)) {
					if (elements.length !== 0) {
						var style = window.getComputedStyle(elements[0]);
						return (style[property]);
					}
				} else {
					elements.forEach(function(elm) {
						elm.style[property] = value;
					});
				}
			},
			'html': function(html) {
				elements.forEach(function(elm) {
					elm.innerHTML = html;
				});
			},
			'text': function(text) {
				if (isNull(text)) {
					if (elements.length !== 0) {
						return (elements[0].innerHTML);
					}
				} else {
					text = document.createTextNode(text);
					elements.forEach(function(elm) {
						elm.innerHTML = '';
						elm.appendChild(text);
					});
				}
			},
			'append': function(element) {
				elements.forEach(function(elm) {
					elm.appendChild(element);
				});
			},
			'prepend': function(element) {
				if (!isNull(element)) {
					elements.forEach(function(elm) {
						elm.insertBefore(element, elm.childNodes[0]);
					});
				}
			},
			'parent': function() {
				if (elements.length !== 0)
					return (elements[0].parentNode);
			},
			'val': function(value) {
				if (isNull(value)) {
					if (elements.length !== 0)
						return (elements[0].value);
				} else
					this.prop('value', value);
			},
			'attr': function(attr, value = null) {
				if (isNull(value)) {
					if (elements.length !== 0)
						return (elements[0].getAttribute(attr));
				} else {
					elements.forEach(function(elm) {
						elm.setAttribute(attr, value);
					});
				}
			},
			'prop': function(property, value = null) {
				if (isNull(value)) {
					if (elements.length !== 0)
						return (elements[0][property]);
				} else {
					elements.forEach(function(elm) {
						elm[property] = value;
					});
				}
			},
			'removeAttr': function(attr) {
				if (!isNull(attr)) {
					elements.forEach(function(elm) {
						elm.removeAttribute(attr);
					});
				}
			},
			'children': function(selector) {
				if (!isNull(selector)) {
					if (elements.length !== 0) {
						return (owner.dom(elements[0].querySelectorAll(selector)));
					}
				}
			}
		}
	}
	owner.dom = defineDom;
})(window);