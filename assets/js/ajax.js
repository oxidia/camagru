// https://www.w3schools.com/js/js_ajax_http_response.asp
'use strict';
(function (owner) {
	function defineAjax() {
		function getXHR() {
			try {
				return new window.XMLHttpRequest();
			}
			catch (e) {
				try {
					return new window.ActiveXObject("Microsoft.XMLHTTP");
				}
				catch (e1) {
					return undefined;
				}
			}
		}

		/*
		params: {
			url: 'example.com',
			params: {
				uname: 'ussef', pass: '1234'
			},
			load: function (text, status) {},
			error: function (status, state) {}
		}
		*/
		function send(method, params) {
			var xhr = getXHR();
			var args = params.params || null;
			var data = method === 'POST' ? new FormData() : '/';

			if (isFunction(params.progress))
				xhr.upload.addEventListener('progress', params.progress);
			if (isFunction(params.load))
				xhr.addEventListener('load', params.load);
			if (isFunction(params.error))
				xhr.addEventListener('error', params.error);
			if (isFunction(params.timeout))
				xhr.addEventListener('timeout', params.timeout);
			if (isFunction(params.abort))
				xhr.addEventListener('abort', params.abort);

			if (args !== null) {
				for (var key in args) {
					if (method === 'POST')
						data.append(key, args[key]);
					else
						data += encodeURIComponent(args[key]) + '/';
				}
			}
			if (method === 'POST')
			{
				xhr.open(method, params.url, params.async || true);
				xhr.send(data);
			}
			else
			{
				xhr.open(method, params.url + data, params.async || true);
				xhr.send();
			}
		}
		return ({
			'get': function (params) {
				try {
					send('GET', params);
				}
				catch (error) {
					console.log(error.message);
				}
			},
			'post': function (params) {
				try {
					send('POST', params);
				}
				catch (error) {
					console.log(error.message);
				}
			}
		});
	}
	owner.AJAX = defineAjax();
})(window);
