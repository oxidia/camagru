dom('#btn_reset').click(function() {
	const   pass = dom('#input_pass').val();
	const   email = dom('#input_email').val();
	const   token = dom('#input_token').val();

	if (!isNull(pass) && !isNull(email) && !isNull(token)) {
		AJAX.post({
			'url': '/reset',
			'params': {  'email': email, 'token': token, 'pass': pass },
			'load': function(event) {
				var data;
				try {
					data = JSON.parse(event.target.responseText);
					setMessage(data, '#message');
				} catch (e) {
					console.log(e.message);
				}
			}
		});
	}
});