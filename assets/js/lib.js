function isFunction(func) {
	return ('function' === typeof func);
}

function redirect(route) {
	window.location.pathname = route;
}

function isNull(object) {
	return (object === null || object === undefined);
}

function setMessage(res, elm_id) {
	var		cls = 'success';
	const	div = dom(elm_id);

	div.removeClass();
	div.addClass('message');
	if (res.error === 1)
		cls = 'error';
	else if (res.error === 2)
		cls = 'info';
	setTimeout(function () {
		div.addClass(cls)
	}, 1);
	div.children('label').text(res.message);
}

function create_comment(text, owner, date, id, delete_handler) {
	var e_comment = document.createElement('div');
	var e_owner = document.createElement('label');
	var e_date = document.createElement('label');
	var e_text = document.createElement('p');
	var e_delete = document.createElement('div');
	var $e_delete = dom(e_delete);
	$e_delete.click(delete_handler);

	dom(e_text).text(text);
	dom(e_text).addClass('text');
	dom(e_owner).text(owner);
	dom(e_owner).addClass('owner');
	dom(e_date).text(date);
	dom(e_date).addClass('date');
	$e_delete.addClass('delete');
	$e_delete.attr('data-role', 'delete');
	$e_delete.attr('data-id', id);
	$e_comment = dom(e_comment);
	$e_comment.addClass('comment');
	$e_comment.append(e_owner);
	$e_comment.append(e_date);
	$e_comment.append(e_text);
	$e_comment.append(e_delete);
	return (e_comment);
}

function create_image(path, id, callable) {
	const	image = document.createElement('div');
	const	img = document.createElement('img');
	const	control = document.createElement('div');
	const	button = document.createElement('button');

	image.className = 'image';
	img.src = '/assets/imgs/users_imgs/' + path;
	control.className = 'control footer'
	button.className = 'button';
	button.setAttribute('data-role', 'delete');
	button.setAttribute('data-id', id);
	button.innerHTML = 'Delete';

	button.addEventListener('click', callable);
	control.appendChild(button);
	image.appendChild(img);
	image.appendChild(control);
	return (image);
}

function forEach(callable) {
	if (!isFunction(callable))
		return ;
	for (var i = 0; i < this.length; i++) {
		callable(this[i]);
	}
}

if (!NodeList.prototype.forEach) {
	NodeList.prototype.forEach = forEach;
}

if (!HTMLCollection.prototype.forEach) {
	HTMLCollection.prototype.forEach = forEach;
}