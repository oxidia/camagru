<?php
ini_set('display_errors', 1);
ini_set('error_reporting', E_ALL);

use Core\Database;

require_once('init.php');

$query_string = file_get_contents(S_CONFIG . '/database.sql');

try
{
	$db_obj = Database::init('mysql:host=127.0.0.1', DB_USER, DB_PASSWORD);
	$db_obj->setAttribute(PDO::ATTR_EMULATE_PREPARES, true);
	$db_obj->nonQuery($query_string);
}
catch (PDOException $e)
{
	print('PDO: ' . $e->getMessage());
}
catch (Exception $e)
{
	print('General: ' . $e->getMessage());
}
