<?php
define('S_ROOT', dirname(__DIR__));
define('S_CONFIG', S_ROOT . '/config');
define('S_APP', S_ROOT . '/App');
define('S_CORE', S_ROOT . '/Core');
define('S_MODELS', S_APP . '/Models');
define('S_VIEWS', S_APP . '/Views');
define('S_INCLUDES', S_APP . '/Includes');
define('S_APIS', S_APP . '/Apis');
define('S_CONTROLLERS', S_APP . '/Controllers');
define('S_ASSETS', S_ROOT . '/assets');
define('S_IMGS', S_ASSETS . '/imgs');
define('S_FILTERS', S_ASSETS . '/imgs/filters');
define('S_USERS_IMGS', S_IMGS . '/users_imgs');

define('C_ROOT', '/');
define('C_ASSETS', C_ROOT . 'assets');
define('C_JS', C_ASSETS . '/js');
define('C_CSS', C_ASSETS . '/css');
define('C_IMGS', C_ASSETS . '/imgs');
define('C_USERS_IMGS', C_IMGS . '/users_imgs');
define('C_ICONS', C_IMGS . '/icons');

define('SERVER_DOMAIN', $_SERVER['SERVER_NAME']);
define('MAX_IMAGES_PAGE', 5);

require_once('database.php');
